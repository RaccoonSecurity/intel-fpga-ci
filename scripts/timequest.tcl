# Check failing paths in TimeQuest
#
# Arguments:
#   path to project qpf
#   project revision

set qpf [lindex ${argv} 0]
set rev [lindex ${argv} 1]

if {[file exist ${qpf}] == 0} {
    error "\nERROR: QPF file \"${qpf}\" not found\n"
}

project_open -force ${qpf} -revision ${rev}

puts "\nINFO: Report failing paths in TimeQuest\n"

set worst_slack_min 0
    
create_timing_netlist
foreach_in_collection op [get_available_operating_conditions] {
    set_operating_conditions ${op}
    read_sdc
    update_timing_netlist
    
    set worst_slack [lindex [report_timing -npaths 1] 1]
    
    set model_cur [get_operating_conditions_info ${op} -model]
    set temp_cur [get_operating_conditions_info ${op} -temperature]
    
    if {${worst_slack} < ${worst_slack_min}} {
        set worst_slack_min ${worst_slack}
    }
    
    puts "\nINFO: Model ${model_cur}, temperature ${temp_cur} C: worst case slack ${worst_slack} ns\n"
}
delete_timing_netlist

project_close

if {${worst_slack_min} < 0} {
    error "\nERROR: Negative worst case slack: ${worst_slack_min} ns\n"
} else {
    puts "\nINFO: Positive worst case slack\n";
}

return 0