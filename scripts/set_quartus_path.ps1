#
# Define Quartus version
#
function Get-Quartus-Version ($file)
{
    $quartus_str = "QUARTUS_VERSION = """
    
    foreach ($line in Get-Content $file)
    {
        $i = $line.IndexOf($quartus_str)
        if ($i -ne -1)
        {
            $start = $i + $quartus_str.Length
            $len = $line.IndexOf("""", $start) - $start
            return $line.Substring($start, $len)
        }
    }
}

#
# Analyze Quartus project file, define Quartus version and sort system path to use needed binaries
#

$file = $args[0]

$version = Get-Quartus-Version $file

if ($version -ge 17.0)
{
    $company = "intelFPGA"
}
else
{
    $company = "altera"
}

# remove last ';' in path
$env:Path = $env:Path.Substring(0, $env:Path.Length - 1)

# sort path and add ';' at the end
$env:Path = (($env:Path.Split(";") | Sort-Object {$_ -like "*$($company)*$($version)*"} -Descending) -join ";") + ';'
return $env:Path