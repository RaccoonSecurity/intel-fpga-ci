# Design implementation
#
# Arguments:
#   path to project qpf
#   project revision

set qpf [lindex ${argv} 0]
set rev [lindex ${argv} 1]

if {[file exist ${qpf}] == 0} {
    error "\nERROR: QPF file \"${qpf}\" not found\n"
}

puts "\nINFO: Run implementation\n"

set status [exec quartus_fit --read_settings_files=off --write_settings_files=off ${qpf} -c ${rev}]

puts ${status}

return 0