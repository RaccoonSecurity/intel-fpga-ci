# Add mem_init reference to qsf
#
# Arguments:
#   path to project qpf
#   project revision
#   workspace
#   app name

set qpf [lindex ${argv} 0]
set rev [lindex ${argv} 1]
set workspace [lindex ${argv} 2]
set app [lindex ${argv} 3]

if {[file exist ${qpf}] == 0} {
    error "\nERROR: QPF file \"${qpf}\" not found\n"
}

set mem_init_qip [file join ${workspace} ${app}]
set mem_init_qip [file join ${mem_init_qip} "mem_init"]
set mem_init_qip [file join ${mem_init_qip} "meminit.qip"]

if {[file exist ${mem_init_qip}] == 0} {
    error "\nERROR: mem_init file \"${mem_init_qip}\" not found\n"
}

project_open -force ${qpf} -revision ${rev}

set project_dir [file dirname ${qpf}]

# absolute path to relative
set mem_init_qip [string range ${mem_init_qip} [expr [string length ${project_dir}] + 1] end]

set_global_assignment -name QIP_FILE ${mem_init_qip}

export_assignments

project_close

return 0