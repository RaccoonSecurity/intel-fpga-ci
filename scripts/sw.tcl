# Build software and generate mem init
#
# Arguments:
#   path to project qpf
#   workspace
#   app name
#   bsp name

set qpf [lindex ${argv} 0]
set workspace [lindex ${argv} 1]
set app [lindex ${argv} 2]
set bsp [lindex ${argv} 3]

if {[file exist ${qpf}] == 0} {
    error "\nERROR: QPF file \"${qpf}\" not found\n"
}
if {[file exist [file join ${workspace} ${app}]] == 0} {
    error "\nERROR: App \"${app}\" not found\n"
}
if {[file exist [file join ${workspace} ${bsp}]] == 0} {
    error "\nERROR: BSP \"${bsp}\" not found\n"
}

set project_dir [file dirname ${qpf}]
set scripts_dir [file dirname [info script]]

source ${scripts_dir}/util.tcl

set sopc_kit_nios2 [get_sopc_kit_nios2_dir]

set nios2_cmd_shell [file join ${sopc_kit_nios2} "Nios II Command Shell.bat"]
set app_dir [file join ${workspace} ${app}]
set bsp_dir [file join ${workspace} ${bsp}]
set settings_bsp [file join ${bsp_dir} "settings.bsp"]

# generate bsp
puts "\nINFO: ${bsp}: generate\n"
set status [exec ${nios2_cmd_shell} nios2-bsp-generate-files --bsp-dir ${bsp_dir} --settings ${settings_bsp}]
puts ${status}

# clean bsp
puts "\nINFO: ${bsp}: clean\n"
set status [exec ${nios2_cmd_shell} make clean -C ${bsp_dir}]
puts ${status}

# make bsp
puts "\nINFO: ${bsp}: make\n"
# successfull bsp build returns error, so we need to use catch
# best way to check success of make is reading of errorCode variable
# when errorCode is "none", no errors occured
# but sometimes I got a corrupted/invalid string in errorCode so I added checking return value of make
set ret [catch {exec ${nios2_cmd_shell} make -C ${bsp_dir}} result opt]
if {[string equal -nocase $::errorCode "none"] == 0 && ${ret} != 0 } {
    puts ${result}
    error "\nERROR: ${bsp}: make error\n"
}
puts ${result}

# clean app
puts "\nINFO: ${app}: clean\n"
set status [exec ${nios2_cmd_shell} make clean -C ${app_dir}]
puts ${status}

# make app
puts "\nINFO: ${app}: make\n"
# successfull app build with warnings returns error, so we need to use catch
set ret [catch {exec ${nios2_cmd_shell} make -C ${app_dir}} result opt]
if {[string equal -nocase $::errorCode "none"] == 0 && ${ret} != 0 } {
    puts ${result}
    error "\nERROR: ${app}: make error\n"
}
puts ${result}

# mem_init generate
puts "\nINFO: ${app}: mem_init_generate\n"
set mem_init_dir [file join ${app_dir} "mem_init"]
# delete it first
if {[file exist ${mem_init_dir}] == 1} {
    file delete -force ${mem_init_dir}
}
# generate
set status [exec ${nios2_cmd_shell} make mem_init_generate -C ${app_dir}]
puts ${status}

return 0