# Generate flash file
#
# Arguments:
#   path to cof file

set cof [lindex ${argv} 0]

if {[file exist ${cof}] == 0} {
    error "\nERROR: COF file \"${cof}\" not found\n"
}

puts "\nINFO: Generate flash\n"

set status [exec quartus_cpf -c ${cof}]

puts ${status}

return 0