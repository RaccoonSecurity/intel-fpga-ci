# Remove all mem_init references in qsf
#
# Arguments:
#   path to project qpf
#   project revision

set qpf [lindex ${argv} 0]
set rev [lindex ${argv} 1]

if {[file exist ${qpf}] == 0} {
    error "\nERROR: QPF file \"${qpf}\" not found\n"
}

project_open -force ${qpf} -revision ${rev}

foreach_in_collection i [get_all_global_assignments -name QIP_FILE] {
    if {[string match "*meminit.qip" [lindex $i 2]]} {
        set_global_assignment -name QIP_FILE [lindex $i 2] -remove
    }
}

export_assignments

project_close

return 0