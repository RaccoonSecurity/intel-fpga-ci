# Design synthesis
#
# Arguments:
#   path to project qpf
#   project revision

set qpf [lindex ${argv} 0]
set rev [lindex ${argv} 1]

if {[file exist ${qpf}] == 0} {
    error "\nERROR: QPF file \"${qpf}\" not found\n"
}

puts "\nINFO: Run synthesis\n"

set status [exec quartus_map --read_settings_files=on --write_settings_files=off ${qpf} -c ${rev}]

puts ${status}

return 0