# Intel FPGA (Altera) build scripts & GitLab CI

## Features

* IP cores generation
* Nios II software build
* FPGA design build
* Flash image generation
* Failing paths check out
* Makefile-based build flow
* Only supported on Windows

## Screenshots

![ci](./upload/ci.png)