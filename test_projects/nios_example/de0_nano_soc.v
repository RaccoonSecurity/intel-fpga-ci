module de0_nano_soc (
    input           FPGA_CLK_50,
    input   [1:0]   KEY
);

wire clk100;

cyc_v_pll cyc_v_pll_inst (
    .refclk     (FPGA_CLK_50),
    .rst        (~KEY[0]),
    .outclk_0   (clk100)
);

nios_system nios_system_inst (
    .clk_clk       (clk100),
    .reset_reset_n (KEY[0])
);


endmodule
