set_time_format -unit ns -decimal_places 3

create_clock -name {FPGA_CLK_50} -period 20.000 -waveform { 0.000 10.000 } [get_ports {FPGA_CLK_50}]

derive_pll_clocks
derive_clock_uncertainty -overwrite