module de0_nano (
    input           CLOCK_50,
    input   [1:0]   KEY
);

wire clk100;

cyc_iv_pll cyc_iv_pll_inst (
    .inclk0     (CLOCK_50),
    .areset     (~KEY[0]),
    .c0         (clk100)
);

nios_system nios_system_inst (
    .clk_clk       (clk100),
    .reset_reset_n (KEY[0])
);


endmodule
